## Centipede Game: Data and Analysis

---

This repository contains all analyses of the centipede game experiment
conducted as part of the manuscript 'Cooperation among an anonymous group protected Bitcoin during failures of decentralization'

- `data_oTree_np_centipede` contains the data files (raw oTree output) from the n-player centipede game experiment

- `data_oTree_2p_centipede` contains the data files (raw oTree output) from the 2-player centipede game experiment

- `analysis.R` contains the _R_ code replicating all analyses

The experiments were run using oTree 3.4.0 by Chen et al. (2016).

All analyses were conducted using R version 4.1.1.